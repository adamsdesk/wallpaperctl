# Wallpaperctl

A light weight script to automatically set and rotate through computer desktop wallpaper. Desktop wallpaper rotation
is based upon a set duration (default, 1 minute). Wallpaper is randomly selected and defaults to look for wallpaper
from the user's home directory under "Pictures/wallpaper/" (e.g. "/home/adam/Pictures/wallpaper").

- [Installation](#installation)
  - [Assumptions](#assumptions)
  - [Dependency Requirements](#dependency-requirements)
  - [Setup](#setup)
- [Configuration](#configuration)
- [Custom Settings](#custom-settings)
  - [Change Wallpaper Directory](#change-wallpaper-directory)
  - [Change Wallpaper Duration](#change-wallpaper-duration)
- [Reference](#reference)
  - [Wallpaper Options](#wallpaper-options)
- [External Links](#external-links)

**Note:** Verified to work on Arch Linux with GNOME v3.14 to v43.

## Installation

### Assumptions

- Have experienced working knowledge within a CLI (command-line interface)
- Understanding of Linux operating system
- Installed all required dependencies as stated in [Dependency Requirements](#dependency-requirements) section
- Installation is done via Linux CLI
- Steps prefixed with a "$" (dollar sign) represents the CLI prompt
- Steps prefixed with a "#" (number sign) represents the CLI prompt with
elevated user permissions (e.g. root)
- The text after the "$" or "#" is to be entered at the CLI
- The installation instructions are an example of installation and configuration

### Dependency Requirements

- BASH v5+
- Git v2+
- GNOME v3.14+

### Setup

1. Clone the project.
    ```console
    $ git clone https://gitlab.com/adouglas/wallpaperctl.git
    ```
1. Change to project directory.
    ```console
    $ cd wallpaperctl
    ```
1. Install Systemd unit files.
    ```console
    # cp etc/systemd/user/wallpaperctl.* /etc/systemd/user/
    ```
1. Install script file.
    ```console
    # cp usr/local/bin/wallpaperctl /usr/local/bin/
    ```
1. Reload the systemd configuration.
    ```console
    # systemctl daemon-reload
    ```
1. Set execute permission.
    ```console
    # chmod +x /usr/local/bin/wallpaperctl
    ```

## Configuration

1. Enable the Systemd timer.
    ```console
    $ systemctl --user enable wallpaperctl.timer
    ```
1. Verify the timer is enabled.
    ```console
    $ systemctl --user list-unit-files
    wallpaperctl.service disabled
    wallpaperctl.timer   enabled
    2 unit files listed.
    ```
1. Start the Systemd timer.
    ```console
    $ systemctl --user start wallpaperctl.timer
    ```
1. Verify the timer has been started.
    ```console
    $ systemctl --user list-timers
    NEXT                         LEFT    LAST                        PASSED UNIT               ACTIVATES
    Thu 2016-03-10 18:49:00 CST  1s left Thu 2016-03-10 18:48:56 CST 1s ago wallpaperctl.timer wallpaperctl.service
    1 timers listed.
    Pass --all to see loaded but inactive timers, too.
    ```

## Custom Settings

### Change Wallpaper Directory

This alteration will set the location of where wallpapers are stored.

1. Create the required directories.
    ```bash
    $ mkdir  ~/.config/systemd ~/.config/systemd/user
    ```
2. Create the configuration file. Replace "username" with applicable value.
    ```bash
    $ nano ~/.config/systemd/user/wallpaperctl.conf
    ```

    WALLPAPER_DIR=/home/username/Pictures/wallpaper

### Change Wallpaper Duration

This alteration will set the duration of how long the wallpaper is displayed for until a new wallpaper is set.

1. Create the required directories.
    ```console
    $ mkdir  -p ~/.config/systemd/user
    ```
1. Create the configuration file.
    ```console
    $ nano ~/.config/systemd/user/wallpaperctl.timer
    ```
1. Change the OnCalendar value as desired.

    Refer to [ArchLinux: systemd/Timers](https://wiki.archlinux.org/index.php/Systemd/Timers#Realtime_timer) for further
    details. The below example changes the duration interval from 1 minute to 5 minutes.

    ```
    [Unit]
    Description=Wallpaperctl - Systemd GNOME wallpaper changer

    [Timer]
    OnCalendar=*:0/5
    Persistent=true
    Unit=wallpaperctl.service

    [Install]
    WantedBy=wallpaperctl.service
    ```

## Reference

### Wallpaper Options

Desktop wallpaper can be displayed in the following ways (org.gnome.desktop.background.picture-options).

**Note:** Not applicable at this time. Here for future use.

- none
- wallpaper
- centered
- scaled
- stretched
- zoom
- spanned

Acceptable display setting values are sourced from:

```console
$ gsettings describe org.gnome.desktop.background picture-options
```
```console
Determines how the image set by wallpaper_filename is rendered. Possible values are “none”, “wallpaper”, “centered”, “scaled”, “stretched”, “zoom”, “spanned”.
```

## External Links

 * [Gsettings with cron - Stack Overflow](https://stackoverflow.com/questions/10374520/gsettings-with-cron)
 * [Rotate GNOME 3’s wallpaper with systemd user units and timers - Major Hayden](https://major.io/2015/02/11/rotate-gnome-3s-wallpaper-systemd-user-units-timers/)
 * [systemd/Timers - ArchWiki](https://wiki.archlinux.org/index.php/Systemd/Timers)
 * [systemd/User - ArchWiki](https://wiki.archlinux.org/index.php/Systemd/User)
